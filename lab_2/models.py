from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=30)
    note_from = models.CharField(max_length=30)
    title = models.CharField(max_length=40)
    message = models.CharField(max_length=60)