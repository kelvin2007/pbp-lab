from django.urls import path
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='lab-4'),
    path('add-note/', add_note, name="add"),
    path('note-list/', note_list, name="notes")
]