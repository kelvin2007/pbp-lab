import 'package:flutter/material.dart';
import 'package:lab_8/models/covid_models.dart';
import 'package:lab_8/data/covid_data.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:lab_8/widgets/main_drawer.dart';

class InfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ViewPageTab(),
    );
  }
}

class ViewPageTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.teal,
        textTheme: ThemeData.light().textTheme.copyWith(
                headline6: const TextStyle(
              fontSize: 26,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            )),
      ),
      initialRoute: '/',
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          drawer: MainDrawer(),
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.stacked_line_chart_rounded),
                  text: "Line Chart",
                ),
                Tab(
                  icon: Icon(Icons.table_chart_rounded),
                  text: "Table View",
                ),
              ],
            ),
            backgroundColor: Colors.teal,
            centerTitle: true,
            title: const Text(
              'Covid Information Page',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          body: TabBarView(
            children: [
              GraphWidget(),
              FormKota(),
            ],
          ),
        ),
      ),
    );
  }
}

class GraphWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder:
            (BuildContext context, AsyncSnapshot<List<CovidData>> dataList) {
          return Container(
            padding: const EdgeInsets.all(10),
            child: SfCartesianChart(
              primaryXAxis: CategoryAxis(),
              title: ChartTitle(
                  text: "Statistik Kasus Harian Per Minggu COVID-19"),
              legend: Legend(isVisible: true, position: LegendPosition.bottom),
              tooltipBehavior: TooltipBehavior(enable: true),
              series: <ChartSeries<CovidData, String>>[
                LineSeries<CovidData, String>(
                  name: 'Kasus Positif',
                  dataSource: dataList.data,
                  xValueMapper: (CovidData data, _) => data.date,
                  yValueMapper: (CovidData data, _) => data.kasusPositif,
                  markerSettings: const MarkerSettings(isVisible: true),
                ),
                LineSeries<CovidData, String>(
                  name: 'Kasus Meninggal',
                  dataSource: dataList.data,
                  xValueMapper: (CovidData data, _) => data.date,
                  yValueMapper: (CovidData data, _) => data.kasusMeninggal,
                  markerSettings: const MarkerSettings(isVisible: true),
                ),
                LineSeries<CovidData, String>(
                  name: 'Kasus Sembuh',
                  dataSource: dataList.data,
                  xValueMapper: (CovidData data, _) => data.date,
                  yValueMapper: (CovidData data, _) => data.kasusSembuh,
                  markerSettings: const MarkerSettings(isVisible: true),
                )
              ],
            ),
          );
        });
  }
}

class FormKota extends StatefulWidget {
  @override
  _FormKotaState createState() => _FormKotaState();
}

class _FormKotaState extends State<FormKota> {
  final _formKey = GlobalKey<FormState>();
  final inputController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    inputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: inputController,
                    decoration: InputDecoration(
                      hintText: "Masukkan Nama Provinsi",
                      icon: const Icon(Icons.location_city),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5)),
                    ),
                    validator: (value) {
                      if (value.isEmpty || value == null) {
                        return 'Nama provinsi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.teal),
                  ),
                  onPressed: () {
                    var pilihan = inputController.text.toString();
                    if (_formKey.currentState.validate()) {
                      showDialog(
                          context: context,
                          barrierDismissible: false, // user must tap button!
                          builder: (BuildContext context) {
                            if (fetchProvinsi(pilihan) != null) {
                              return AlertDialog(
                                  title: Container(
                                    child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: FutureBuilder(
                                            future: fetchProvinsi(pilihan),
                                            builder: (BuildContext context,
                                                AsyncSnapshot<CovidProvinsi>
                                                    provdata) {
                                              if (provdata.hasData &&
                                                  provdata.data != null) {
                                                return Text(
                                                  'Kasus Kumulatif Untuk Provinsi ${provdata.data.provinsi}',
                                                  style: const TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                );
                                              } else {
                                                return const CircularProgressIndicator();
                                              }
                                            })),
                                    color: Colors.teal,
                                  ),
                                  content: Container(
                                    height: 200,
                                    width: 300,
                                    child: ListView(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.vertical,
                                        children: [
                                          SingleChildScrollView(
                                              primary: true,
                                              physics:
                                                  AlwaysScrollableScrollPhysics(),
                                              scrollDirection: Axis.horizontal,
                                              child: FutureBuilder(
                                                  future:
                                                      fetchProvinsi(pilihan),
                                                  builder:
                                                      (BuildContext context,
                                                          AsyncSnapshot<
                                                                  CovidProvinsi>
                                                              provdata) {
                                                    if (provdata.hasData &&
                                                        provdata.data != null) {
                                                      return DataTable(
                                                          columns: const [
                                                            DataColumn(
                                                                label: Text(
                                                                    'Kasus Positif',
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            14,
                                                                        fontWeight:
                                                                            FontWeight.bold))),
                                                            DataColumn(
                                                                label: Text(
                                                                    'Kasus Meninggal',
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            14,
                                                                        fontWeight:
                                                                            FontWeight.bold))),
                                                            DataColumn(
                                                                label: Text(
                                                                    'Kasus Sembuh',
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            14,
                                                                        fontWeight:
                                                                            FontWeight.bold))),
                                                          ],
                                                          rows: [
                                                            DataRow(cells: [
                                                              DataCell(
                                                                Text(provdata
                                                                    .data
                                                                    .kumulatifPositif
                                                                    .toString()),
                                                              ),
                                                              DataCell(
                                                                Text(provdata
                                                                    .data
                                                                    .kumulatifMeninggal
                                                                    .toString()),
                                                              ),
                                                              DataCell(
                                                                Text(provdata
                                                                    .data
                                                                    .kumulatifSembuh
                                                                    .toString()),
                                                              ),
                                                            ])
                                                          ]);
                                                    } else {
                                                      return const CircularProgressIndicator();
                                                    }
                                                  })),
                                          const Padding(
                                            padding: EdgeInsets.all(10),
                                            child: Text(
                                              '* Geser ke kanan dan ke kiri untuk melihat dengan detail',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 10,
                                                  fontStyle: FontStyle.italic),
                                            ),
                                          )
                                        ]),
                                  ));
                            } else {
                              return AlertDialog(
                                title: Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'Provinsi Tidak Ditemukan',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  color: Colors.red,
                                ),
                              );
                            }
                          });
                    } else {
                      return null;
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
