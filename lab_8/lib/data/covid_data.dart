import 'package:lab_8/models/covid_models.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<CovidProvinsi> fetchProvinsi(String query) async {
  CovidProvinsi provinsi;
  const url = 'http://127.0.0.1:8000/covid-info/get-provinsi/';
  try {
    print(query);
    final response = await http.post(Uri.parse(url),
        body: jsonEncode({
          "query": query,
        }));
    var extractedData = jsonDecode(response.body);

    provinsi = CovidProvinsi(
        provinsi: extractedData['provinsi'],
        kumulatifPositif: extractedData['positif'],
        kumulatifMeninggal: extractedData['meninggal'],
        kumulatifSembuh: extractedData['sembuh']);
  } catch (error) {
    print(error);
  }
  return provinsi;
}

Future<List<CovidData>> fetchData() async {
  List<CovidData> dataIndonesia = [];
  List<String> label;
  List<int> kasusPositif;
  List<int> kasusMeninggal;
  List<int> kasusSembuh;
  const url = 'http://127.0.0.1:8000/covid-info/api-indonesia/';
  try {
    final response = await http.get(Uri.parse(url));
    Map<String, dynamic> extractedData = jsonDecode(response.body);
    label = extractedData['labels'].cast<String>();
    kasusPositif = extractedData['dataPositif'].cast<int>();
    kasusMeninggal = extractedData['dataMeninggal'].cast<int>();
    kasusSembuh = extractedData['dataRecovery'].cast<int>();
    dataIndonesia = [
      CovidData(
          date: label[0],
          kasusPositif: kasusPositif[0],
          kasusMeninggal: kasusMeninggal[0],
          kasusSembuh: kasusSembuh[0]),
      CovidData(
          date: label[1],
          kasusPositif: kasusPositif[1],
          kasusMeninggal: kasusMeninggal[1],
          kasusSembuh: kasusSembuh[1]),
      CovidData(
          date: label[2],
          kasusPositif: kasusPositif[2],
          kasusMeninggal: kasusMeninggal[2],
          kasusSembuh: kasusSembuh[2]),
      CovidData(
          date: label[3],
          kasusPositif: kasusPositif[3],
          kasusMeninggal: kasusMeninggal[3],
          kasusSembuh: kasusSembuh[3]),
      CovidData(
          date: label[4],
          kasusPositif: kasusPositif[4],
          kasusMeninggal: kasusMeninggal[4],
          kasusSembuh: kasusSembuh[4]),
      CovidData(
          date: label[5],
          kasusPositif: kasusPositif[5],
          kasusMeninggal: kasusMeninggal[5],
          kasusSembuh: kasusSembuh[5]),
      CovidData(
          date: label[6],
          kasusPositif: kasusPositif[6],
          kasusMeninggal: kasusMeninggal[6],
          kasusSembuh: kasusSembuh[6]),
    ];
  } catch (error) {
    print(error);
  }
  return dataIndonesia;
}
