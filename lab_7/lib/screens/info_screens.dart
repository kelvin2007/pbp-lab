import 'package:flutter/material.dart';
import 'package:lab_7/models/covid_models.dart';
import 'package:lab_7/data/covid_data.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:lab_7/widgets/main_drawer.dart';

class InfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ViewPageTab(),
    );
  }
}

class ViewPageTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.teal,
        textTheme: ThemeData.light().textTheme.copyWith(
                headline6: TextStyle(
              fontSize: 26,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            )),
      ),
      initialRoute: '/',
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          drawer: MainDrawer(),
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.stacked_line_chart_rounded),
                  text: "Line Chart",
                ),
                Tab(
                  icon: Icon(Icons.table_chart_rounded),
                  text: "Table View",
                ),
              ],
            ),
            backgroundColor: Colors.teal,
            centerTitle: true,
            title: const Text(
              'Covid Information Page',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          body: TabBarView(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                child: SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  title: ChartTitle(
                      text: "Statistik Kasus Harian Per Minggu COVID-19"),
                  legend:
                      Legend(isVisible: true, position: LegendPosition.bottom),
                  tooltipBehavior: TooltipBehavior(enable: true),
                  series: <ChartSeries<CovidData, String>>[
                    LineSeries<CovidData, String>(
                      name: 'Kasus Positif',
                      dataSource: dataList,
                      xValueMapper: (CovidData data, _) => data.date,
                      yValueMapper: (CovidData data, _) => data.kasusPositif,
                      markerSettings: MarkerSettings(isVisible: true),
                    ),
                    LineSeries<CovidData, String>(
                      name: 'Kasus Meninggal',
                      dataSource: dataList,
                      xValueMapper: (CovidData data, _) => data.date,
                      yValueMapper: (CovidData data, _) => data.kasusMeninggal,
                      markerSettings: MarkerSettings(isVisible: true),
                    ),
                    LineSeries<CovidData, String>(
                      name: 'Kasus Sembuh',
                      dataSource: dataList,
                      xValueMapper: (CovidData data, _) => data.date,
                      yValueMapper: (CovidData data, _) => data.kasusSembuh,
                      markerSettings: MarkerSettings(isVisible: true),
                    )
                  ],
                ),
              ),
              new FormKota(),
            ],
          ),
        ),
      ),
    );
  }
}

class FormKota extends StatefulWidget {
  @override
  _FormKotaState createState() => _FormKotaState();
}

class _FormKotaState extends State<FormKota> {
  final _formKey = GlobalKey<FormState>();
  final inputController = TextEditingController();

  CovidProvinsi getData(String nama) {
    for (int i = 0; i < kasusProvinsi.length; i++) {
      if (kasusProvinsi[i].provinsi.toLowerCase() == nama.toLowerCase()) {
        return kasusProvinsi[i];
      }
    }
    return null;
  }

  void dispose() {
    // Clean up the controller when the widget is disposed.
    inputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: inputController,
                    decoration: new InputDecoration(
                      hintText: "Masukkan Nama Provinsi",
                      icon: Icon(Icons.location_city),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5)),
                    ),
                    validator: (value) {
                      if (value.isEmpty || value == null) {
                        return 'Nama provinsi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.teal),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      showDialog(
                          context: context,
                          builder: (context) {
                            if (getData(inputController.text) != null) {
                              return AlertDialog(
                                  title: Container(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        'Kasus Kumulatif Untuk Provinsi ${getData(inputController.text.toString()).provinsi}',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    color: Colors.teal,
                                  ),
                                  content: Container(
                                      height: 200,
                                      width: 300,
                                      child: ListView(
                                          scrollDirection: Axis.vertical,
                                          children: [
                                            SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: DataTable(columns: [
                                                DataColumn(
                                                    label: Text('Kasus Positif',
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold))),
                                                DataColumn(
                                                    label: Text(
                                                        'Kasus Meninggal',
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold))),
                                                DataColumn(
                                                    label: Text('Kasus Sembuh',
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold))),
                                              ], rows: [
                                                DataRow(cells: [
                                                  DataCell(
                                                    Text(getData(inputController
                                                            .text
                                                            .toString())
                                                        .kumulatifPositif
                                                        .toString()),
                                                  ),
                                                  DataCell(
                                                    Text(getData(inputController
                                                            .text
                                                            .toString())
                                                        .kumulatifMeninggal
                                                        .toString()),
                                                  ),
                                                  DataCell(
                                                    Text(getData(inputController
                                                            .text
                                                            .toString())
                                                        .kumulatifSembuh
                                                        .toString()),
                                                  ),
                                                ])
                                              ]),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(10),
                                              child: Text(
                                                '* Geser ke kanan dan ke kiri untuk melihat dengan detail',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 10,
                                                    fontStyle:
                                                        FontStyle.italic),
                                              ),
                                            )
                                          ])));
                            } else {
                              return AlertDialog(
                                title: Container(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Provinsi Tidak Ditemukan',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  color: Colors.red,
                                ),
                              );
                            }
                          });
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
