import 'package:flutter/material.dart';
import 'package:lab_6/models/covid_models.dart';
import 'package:lab_6/data/covid_data.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:lab_6/widgets/main_drawer.dart';

class InfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ViewPageTab(),
    );
  }
}

// class Table extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ;
//   }
// }

class ViewPageTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.teal,
        textTheme: ThemeData.light().textTheme.copyWith(
                headline6: TextStyle(
              fontSize: 26,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            )),
      ),
      initialRoute: '/',
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          drawer: MainDrawer(),
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.stacked_line_chart_rounded),
                  text: "Line Chart",
                ),
                Tab(
                  icon: Icon(Icons.table_chart_rounded),
                  text: "Table View",
                ),
              ],
            ),
            backgroundColor: Colors.teal,
            centerTitle: true,
            title: const Text(
              'Covid Information Page',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          body: TabBarView(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                child: SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  title: ChartTitle(
                      text: "Statistik Kasus Harian Per Minggu COVID-19"),
                  legend:
                      Legend(isVisible: true, position: LegendPosition.bottom),
                  tooltipBehavior: TooltipBehavior(enable: true),
                  series: <ChartSeries<CovidData, String>>[
                    LineSeries<CovidData, String>(
                      name: 'Kasus Positif',
                      dataSource: dataList,
                      xValueMapper: (CovidData data, _) => data.date,
                      yValueMapper: (CovidData data, _) => data.kasusPositif,
                      markerSettings: MarkerSettings(isVisible: true),
                    ),
                    LineSeries<CovidData, String>(
                      name: 'Kasus Meninggal',
                      dataSource: dataList,
                      xValueMapper: (CovidData data, _) => data.date,
                      yValueMapper: (CovidData data, _) => data.kasusMeninggal,
                      markerSettings: MarkerSettings(isVisible: true),
                    ),
                    LineSeries<CovidData, String>(
                      name: 'Kasus Sembuh',
                      dataSource: dataList,
                      xValueMapper: (CovidData data, _) => data.date,
                      yValueMapper: (CovidData data, _) => data.kasusSembuh,
                      markerSettings: MarkerSettings(isVisible: true),
                    )
                  ],
                ),
              ),
              ListView(scrollDirection: Axis.vertical, children: [
                Container(
                  padding: EdgeInsets.all(10),
                  child: Center(
                    child: Text(
                      "Kasus Kumulatif COVID-19 Per Provinsi",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: DataTable(
                      columns: [
                        DataColumn(
                            label: Text('Provinsi',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold))),
                        DataColumn(
                            label: Text('Kasus Positif',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold))),
                        DataColumn(
                            label: Text('Kasus Meninggal',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold))),
                        DataColumn(
                            label: Text('Kasus Sembuh',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold))),
                      ],
                      rows: kasusProvinsi
                          .map((data) => DataRow(cells: [
                                DataCell(
                                  Text(data.provinsi),
                                ),
                                DataCell(
                                  Text(data.kumulatifPositif.toString()),
                                ),
                                DataCell(
                                  Text(data.kumulatifMeninggal.toString()),
                                ),
                                DataCell(
                                  Text(data.kumulatifSembuh.toString()),
                                ),
                              ]))
                          .toList()),
                )
              ])
            ],
          ),
        ),
      ),
    );
  }
}
