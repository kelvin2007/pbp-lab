import 'package:lab_6/models/covid_models.dart';

List<CovidProvinsi> kasusProvinsi = [
  CovidProvinsi(
      provinsi: "DKI Jakarta",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "Aceh",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "Sumatera Barat",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "Kalimantan Selatan",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "Sulawesi Selatan",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "Maluku",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "DI Yogyakarta",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "Bali",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
  CovidProvinsi(
      provinsi: "Jawa Barat",
      kumulatifPositif: 100,
      kumulatifMeninggal: 100,
      kumulatifSembuh: 100),
];

List<CovidData> dataList = [
  CovidData(
      date: "2021-11-15", kasusPositif: 3, kasusMeninggal: 2, kasusSembuh: 4),
  CovidData(
      date: "2021-11-16", kasusPositif: 3, kasusMeninggal: 2, kasusSembuh: 5),
  CovidData(
      date: "2021-11-17", kasusPositif: 3, kasusMeninggal: 2, kasusSembuh: 7),
  CovidData(
      date: "2021-11-18", kasusPositif: 3, kasusMeninggal: 2, kasusSembuh: 10),
  CovidData(
      date: "2021-11-19", kasusPositif: 3, kasusMeninggal: 2, kasusSembuh: 4),
  CovidData(
      date: "2021-11-20", kasusPositif: 3, kasusMeninggal: 2, kasusSembuh: 7),
  CovidData(
      date: "2021-11-21", kasusPositif: 3, kasusMeninggal: 2, kasusSembuh: 8),
];
