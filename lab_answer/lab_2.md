# Jawaban Tugas Lab 2: Data Delivery Using HTML, XML, and JSON

## Nomor 1
1. **Apakah perbedaan antara JSON dan XML?** <br />
JSON dan XML adalah format pertukaran data yang seringkali kita dengar. Keduanya memiliki beberapa perbedaan sebagai berikut.
- Sesuai dengan kepanjangan dari JSON itu sendiri, JavaScript Object Notation (JSON) berbasis bahasa pemrograman JavaScript, sedangkan Extensible Markup Language (XML) berbasis bahasa *markup* atau lebih tepatnya SGML (Standard Generalized Markup Language).
- JSON merepresentasikan data sebagai objek, XML merepresentasikan data sebagai tag.
- Atribut Objek pada JSON memiliki tipe data, seperti integer, string, dan beberapa tipe data lainnya. Di sisi lain, XML tidak memiliki tipe data dan semua data di XML adalah string.

## Nomor 2
2. **Apakah perbedaan antara HTML dan XML?**
- Walapun keduanya sama-sama *markup language*, XML lebih fleksibel dalam memakai *tag* karena *tag* pada XML dapat didefinisikan langsung oleh *programmer*, sedangkan HTML terbatas pada *tag* yang sudah ada.
- HTML digunakan untuk menampilkan data pada *user*, XML digunakan untuk men-*transfer* data antarsistem.
- Karena XML digunakan untuk men-*transfer* data, XML memiliki sifat *case sensitive*, sedangkan HTML *case insensitive*.
- *Tag* pada XML wajib menggunakan *closing tag*, sedangkan HTML opsional.