from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.BigIntegerField(default=0)
    dob = models.DateField(default=timezone.now)
